package com.edso.pawnservice.Common;

public enum CollateralStatus {
    PROCESSING_CREATE,
    FAIL_CREATE,
    OPEN,
    PROCESSING_ACCEPT,
    PROCESSING_WITHDRAW,
    ACCEPTED,
    WITHDRAWN,
    FAILED
}
