package com.edso.pawnservice.Common;

public enum OfferStatus {
    PROCESSING_CREATE,
    FAILED_CREATE,
    OPEN,
    PROCESSING_ACCEPT,
    PROCESSING_REJECT,
    PROCESSING_CANCEL,
    ACCEPTED,
    REJECTED,
    CANCELED
}
