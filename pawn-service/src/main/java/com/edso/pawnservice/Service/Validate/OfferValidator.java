package com.edso.pawnservice.Service.Validate;

import com.edso.pawnservice.Common.DurationStatus;
import com.edso.pawnservice.Exception.CustomException;
import com.edso.pawnservice.Exception.ListValidationException;
import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Request.OfferRequest;
import com.edso.pawnservice.Repository.CollateralRepository;
import com.edso.pawnservice.Repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class OfferValidator {

    @Autowired
    private CollateralRepository collateralRepository;

    @Autowired
    private ListValidationException listValidationException;

    @Autowired
    private CryptoRepository cryptoRepository;

    public void validate(OfferRequest offerRequest) {
        List<ValidationException> listError = listValidationException.getValidationExceptions();
        listError.clear();

        Optional<Collateral> optionalCollateral;
        // kiểm tra collateral null
        if (Objects.isNull(offerRequest.getCollateralId())) {
            throw new CustomException("Invalid collateral id");
        } else {
            // Kiểm tra liệu collateral tồn tại trong db không
            optionalCollateral = collateralRepository.findById(offerRequest.getCollateralId());
            if (optionalCollateral.isEmpty()) {
                throw new CustomException("Collateral isn't found");
            }
        }

        // Kiểm tra liquidation threshold null
        if (Objects.isNull(offerRequest.getLiquidationThreshold())) {
            listError.add(new ValidationException("Invalid Liquidation Threshold"));
        }

        // Kiểm tra duration vs recurringInterest = weekly
        if (Objects.isNull(offerRequest.getDurationType()) ||
                offerRequest.getDurationType().equals(DurationStatus.WEEK.toString()) &&
                        offerRequest.getDurationTime() > 5200) {
            listError.add(new ValidationException("Duration by week can not greater than 5200"));
        }

        // Kiểm tra duration vs recurringInterest = monthly , duration null
        if (Objects.isNull(offerRequest.getDurationType()) ||
                offerRequest.getDurationType().equals(DurationStatus.MONTH.toString()) &&
                        offerRequest.getDurationTime() > 1200) {
            listError.add(new ValidationException("Duration by month can not greater than 1200"));
        }

        if (listError.size() > 0) {
            throw listValidationException;
        }
    }
}
