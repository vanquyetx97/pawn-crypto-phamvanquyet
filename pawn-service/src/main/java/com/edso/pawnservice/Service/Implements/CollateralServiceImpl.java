package com.edso.pawnservice.Service.Implements;

import com.edso.pawnservice.Common.CollateralStatus;
import com.edso.pawnservice.Common.OfferStatus;
import com.edso.pawnservice.Exception.CustomException;
import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Entity.Offer;
import com.edso.pawnservice.Model.Request.CollateralRequest;
import com.edso.pawnservice.Model.Response.CollateralResponse;
import com.edso.pawnservice.Model.Response.ResponseEntity;
import com.edso.pawnservice.Repository.CollateralRepository;
import com.edso.pawnservice.Repository.OfferRepository;
import com.edso.pawnservice.Service.CollateralService;
import com.edso.pawnservice.Service.CryptoService;
import com.edso.pawnservice.Service.Mapper.CollateralMapper;
import com.edso.pawnservice.Service.Validate.CollateralValidator;
import com.edso.pawnservice.Specification.CollateralSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CollateralServiceImpl implements CollateralService {

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private CollateralRepository collateralRepository;

    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private CollateralMapper collateralMapper;

    @Autowired
    private CollateralValidator validator;

    /**
     * Lấy tất cả Collateral
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CollateralResponse> getAll(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize);
        Page<Collateral> page = collateralRepository.findAll(paging);
        List<Collateral> collaterals = page.getContent();
        return collateralMapper.mapToListResponse(collaterals);
    }

    /**
     * lấy về entity Collateral theo id
     *
     * @param id
     * @return
     */
    @Override
    public Collateral getCollateralEntity(Long id) {
        Optional<Collateral> optional = collateralRepository.findById(id);
        if (optional.isEmpty()) {
            throw new CustomException("Collateral resource not found");
        }
        return optional.get();
    }

    /**
     * lấy ra Collateral theo id
     *
     * @param id
     * @return
     */
    @Override
    public CollateralResponse getById(Long id) {
        // kiểm tra Collateral có tồn tại
        Optional<Collateral> opt = collateralRepository.findById(id);
        if (opt.isEmpty()) {
            throw new CustomException("Collateral not found");
        }

        Collateral collateral = opt.get();
        // Tính toán estimate
        BigDecimal estimate = evaluateEstimate(collateral);
        CollateralResponse collateralResponse = collateralMapper.mapToCollateralResponse(collateral);
        collateralResponse.setEstimate(estimate);
        return collateralResponse;
    }

    /**
     * Thực hiện tính toán estimate
     *
     * @param collateral
     * @return
     */
    @Override
    public BigDecimal evaluateEstimate(Collateral collateral) {
        // lấy amount hiện tại của Collateral
        BigDecimal amount = collateral.getCollateralAmount();
        // lấy dữ liệu được gọi từ Crypto-Service
        ResponseEntity responseEntity = cryptoService.getCurrentPriceOfSymbol(collateral.getCryptoCurrency().getSymbol());
        LinkedHashMap data = (LinkedHashMap) responseEntity.getData();
        System.out.println("responseEntity.getData: " + responseEntity.getData());

        BigDecimal price = BigDecimal.valueOf((Double.parseDouble(data.get("currentPrice").toString())));
        BigDecimal estimate = amount.multiply(price);
        return estimate;
    }

    /**
     * Tạo mới 1 Collateral
     *
     * @param collateralRequest
     * @return
     */
    @Override
    public CollateralRequest createCollateral(CollateralRequest collateralRequest) {
        // Validate đầu vào
        validator.validate(collateralRequest);
        collateralRequest.setStatus((CollateralStatus.OPEN.toString()));
        Collateral collateral = collateralMapper.mapToCollateral(collateralRequest);
        collateralRepository.save(collateral);
        return new CollateralRequest();
    }

    /**
     * update Collateral theo id
     *
     * @param id
     * @param collateralRequest
     * @return
     */
    @Override
    public CollateralResponse updateCollateralById(Long id, CollateralRequest collateralRequest) {
        Collateral collateral = collateralMapper.mapToCollateral(collateralRequest);
        collateral.setId(id);
        return saveAndReturnResponse(collateral);
    }

    /**
     * Lưu dữ liệu từ Entity về Response
     *
     * @param collateral
     * @return
     */
    private CollateralResponse saveAndReturnResponse(Collateral collateral) {
        Collateral savedCollateral = collateralRepository.save(collateral);
        return collateralMapper.mapToCollateralResponse(savedCollateral);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public CollateralResponse withDrawCollateral(Long id) {
        Optional<Collateral> optional = collateralRepository.findById(id);
        if (optional.isEmpty()) {
            throw new CustomException("Collateral not found");
        }
        Collateral collateral = optional.get();
        if (!Objects.equals(collateral.getStatus(), CollateralStatus.OPEN.toString())) {
            throw new CustomException("Collateral is it OPEN");
        }
        collateral.setStatus(CollateralStatus.WITHDRAWN.toString());
        List<Offer> offers = collateral.getOffers();
        offers.forEach(offer -> {
            offer.setStatus(OfferStatus.CANCELED.toString());
            offerRepository.saveAndFlush(offer);
        });
        collateralRepository.saveAndFlush(collateral);
        return collateralMapper.mapToCollateralResponse(collateral);

    }

    /**
     * Tìm kiếm theo walletAddress, loanCurrencyCryptoId, collateralCryptoId, status
     *
     * @param walletAddress
     * @param loanCurrencyCryptoId
     * @param collateralCryptoId
     * @param status
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CollateralResponse> filterCollateral(String walletAddress,
                                                     Long loanCurrencyCryptoId,
                                                     Long collateralCryptoId,
                                                     String status,
                                                     int pageIndex,
                                                     int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        Page<Collateral> page = collateralRepository.findAll(CollateralSpecification.filterForCollateral(walletAddress,
                loanCurrencyCryptoId, collateralCryptoId, status), pageable);
        List<Collateral> collaterals = page.getContent();
        return collateralMapper.mapToListResponse(collaterals);

    }

    /**
     * Cập nhật 1 offer
     *
     * @param collateralId
     * @param offerId
     * @return
     */
    @Override
    public CollateralResponse acceptOfferForCollateral(Long collateralId, Long offerId) {
        Collateral collateral = collateralRepository.findById(collateralId).orElseThrow(()
                -> new CustomException("Collateral resource not found"));
        if (!Objects.equals(collateral.getStatus(), (CollateralStatus.OPEN.toString()))) {
            throw new CustomException("Collateral isn't open !");
        }
        collateral.setStatus((CollateralStatus.ACCEPTED.toString()));
        List<Offer> offersOfCollateral = collateral.getOffers();
        offersOfCollateral.forEach(offer -> {
            if (offer.getId() == offerId) {
                offer.setStatus((OfferStatus.ACCEPTED.toString()));
            } else {
                offer.setStatus((OfferStatus.REJECTED.toString()));
            }
            offerRepository.saveAndFlush(offer);
        });
        return collateralMapper.mapToCollateralResponse(collateral);
    }

}
