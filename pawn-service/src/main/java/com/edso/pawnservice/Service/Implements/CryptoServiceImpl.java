package com.edso.pawnservice.Service.Implements;

import com.edso.pawnservice.Exception.CustomException;
import com.edso.pawnservice.Model.Entity.Crypto;
import com.edso.pawnservice.Model.Response.ResponseEntity;
import com.edso.pawnservice.Repository.CryptoRepository;
import com.edso.pawnservice.Service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;

@Service
public class CryptoServiceImpl implements CryptoService {

    @Autowired
    CryptoRepository cryptoRepository;

    private final WebClient webClient;

    private final String CRYPTO_SERVICE_URL ="http://localhost:9002";
    private final String CRYPTO_SERVICE_URI = "/crypto/current-price?symbol={symbol}";

    public CryptoServiceImpl() {
        this.webClient = WebClient.create(CRYPTO_SERVICE_URL);
    }

    /**
     * Lấy tỷ giá của crypto
     *
     * @param symbol : tên symbol của crypto
     * @return
     */
    @Override
    public ResponseEntity getCurrentPriceOfSymbol(String symbol) {
        ResponseEntity response = webClient.get()
                .uri(CRYPTO_SERVICE_URI,symbol)
                .retrieve()
                .bodyToMono(ResponseEntity.class)
                .block();
        return response;
    }

    /**
     * Lấy thông tin Crypto dựa theo Id, check tồn tại
     *
     * @param id
     * @return
     */
    public Crypto findCryptoWithId(Long id) {
        Optional<Crypto> opt = cryptoRepository.findById(id);
        if (opt.isEmpty()) {
            return opt.get();
        }
        throw new CustomException("Crypto is it found");
    }


}
