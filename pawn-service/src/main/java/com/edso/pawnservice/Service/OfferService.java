package com.edso.pawnservice.Service;

import com.edso.pawnservice.Model.Request.OfferRequest;
import com.edso.pawnservice.Model.Response.OfferResponse;

public interface OfferService {

    OfferResponse getById(Long id);

    OfferResponse createOffer(OfferRequest offerRequest);

}
