package com.edso.pawnservice.Service;

import com.edso.pawnservice.Model.Entity.Crypto;
import com.edso.pawnservice.Model.Response.ResponseEntity;

public interface CryptoService {

    ResponseEntity getCurrentPriceOfSymbol(String symbol);

    Crypto findCryptoWithId(Long id);
}
