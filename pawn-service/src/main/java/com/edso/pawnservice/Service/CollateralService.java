package com.edso.pawnservice.Service;

import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Request.CollateralRequest;
import com.edso.pawnservice.Model.Response.CollateralResponse;

import java.math.BigDecimal;
import java.util.List;

public interface CollateralService {

    List<CollateralResponse> getAll(int pageIndex, int pageSize);

    CollateralRequest createCollateral(CollateralRequest collateralRequest);

    CollateralResponse updateCollateralById(Long id, CollateralRequest collateralRequest);

    CollateralResponse getById(Long id);

    CollateralResponse withDrawCollateral(Long id);

    List<CollateralResponse> filterCollateral(String walletAddress, Long loanCurrencyCryptoId,
                                              Long collateralCryptoId, String status, int pageIndex, int pageSize);

    Collateral getCollateralEntity(Long id);

    CollateralResponse acceptOfferForCollateral(Long collateralId, Long offerId);

    BigDecimal evaluateEstimate(Collateral collateral);
}
