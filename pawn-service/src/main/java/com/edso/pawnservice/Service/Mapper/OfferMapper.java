package com.edso.pawnservice.Service.Mapper;

import com.edso.pawnservice.Common.OfferStatus;
import com.edso.pawnservice.Model.Entity.Offer;
import com.edso.pawnservice.Model.Request.OfferRequest;
import com.edso.pawnservice.Model.Response.OfferResponse;
import com.edso.pawnservice.Service.CollateralService;
import com.edso.pawnservice.Service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OfferMapper {

    @Autowired
    private CollateralService collateralService;
    @Autowired
    private CryptoService cryptoService;

    /**
     * Convert dữ liệu từ Entity sang Response
     *
     * @param offer
     * @return
     */
    public OfferResponse mapToResponse(Offer offer) {
        OfferResponse offerResponse = new OfferResponse();
        offerResponse.setId(offer.getId());
        offerResponse.setMessage(offer.getMessage());
        offerResponse.setStatus(offer.getStatus());
        offerResponse.setLoanAmount(offer.getLoanAmount());
        offerResponse.setDurationTime(offer.getDurationTime());
        offerResponse.setDurationType(offer.getDurationType());
        offerResponse.setLoanToValue(offer.getLoanToValue());
        offerResponse.setLiquidationThreshold(offer.getLiquidationThreshold());
        offerResponse.setInterestRate(offer.getInterestRate());
        offerResponse.setCryptoId(offer.getCrypto().getId());
        offerResponse.setCollateralId(offer.getCollateral().getId());
        offerResponse.setWalletAddress(offer.getWalletAddress());
        offerResponse.setCreatedAt(offer.getCreatedAt());
        return offerResponse;
    }

    /**
     * Convert dữ liệu từ Request sang Entity
     *
     * @param offerRequest
     * @return
     */
    public Offer mapToOfferEntity(OfferRequest offerRequest) {
        Offer offer = new Offer();
        offer.setStatus(offerRequest.getStatus());
        offer.setLiquidationThreshold(offerRequest.getLiquidationThreshold());
        offer.setMessage(offerRequest.getMessage());
        offer.setInterestRate(offerRequest.getInterestRate());
        offer.setLoanAmount(offerRequest.getLoanAmount());
        offer.setLoanToValue(offerRequest.getLoanToValue());
        offer.setDurationType(offerRequest.getDurationType());
        offer.setDurationTime(offerRequest.getDurationTime());
        offer.setWalletAddress(offerRequest.getWalletAddress());
        offer.setCollateral(collateralService.getCollateralEntity(offerRequest.getCollateralId()));
        offer.setCrypto(cryptoService.findCryptoWithId(offerRequest.getCryptoId()));
        offer.setStatus(OfferStatus.OPEN.toString());
        return offer;
    }

    public List<OfferResponse> mapToList(List<Offer> offers) {
        List<OfferResponse> offerResponseList = new ArrayList<>();
        offers.forEach(offer -> offerResponseList.add(mapToResponse(offer)));
        return offerResponseList;
    }

}
