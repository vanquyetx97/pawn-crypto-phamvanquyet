package com.edso.pawnservice.Service.Validate;

import com.edso.pawnservice.Common.DurationStatus;
import com.edso.pawnservice.Exception.ListValidationException;
import com.edso.pawnservice.Model.Request.CollateralRequest;
import com.edso.pawnservice.Repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.List;

@Component
public class CollateralValidator {

    @Autowired
    private ListValidationException listException;

    /**
     * Hàm validate DurationType
     *
     * @param collateralRequest
     */
    public void validate(CollateralRequest collateralRequest) {
        List<ValidationException> listError = listException.getValidationExceptions();
        listError.clear();
        // Check DurationType có hợp lệ không
        if (!collateralRequest.getDurationType().equals(DurationStatus.WEEK.toString())
                && !collateralRequest.getDurationType().equals(DurationStatus.MONTH.toString())) {
            listError.add(new ValidationException("Invalid DurationType"));
        }
        // Check số lượng DurationType nhập vào từ Request theo WEEK
        if (collateralRequest.getDurationType().equals(DurationStatus.WEEK.toString())
                && collateralRequest.getDurationTime() > 5200) {
            listError.add(new ValidationException("Duration by week can not greater than 5200"));
        }
        // Check số lượng DurationType nhập vào từ Request theo MONTH
        if (collateralRequest.getDurationType().equals(DurationStatus.MONTH.toString())
                && collateralRequest.getDurationTime() > 1200) {
            listError.add(new ValidationException("Duration by month can not greater than 1200"));
        }
        if (listError.size() > 0) {
            throw listException;
        }
    }

}
