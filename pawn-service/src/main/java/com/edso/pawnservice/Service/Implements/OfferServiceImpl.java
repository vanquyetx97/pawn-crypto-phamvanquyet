package com.edso.pawnservice.Service.Implements;

import com.edso.pawnservice.Exception.CustomException;
import com.edso.pawnservice.Model.Entity.Offer;
import com.edso.pawnservice.Model.Request.OfferRequest;
import com.edso.pawnservice.Model.Response.OfferResponse;
import com.edso.pawnservice.Repository.OfferRepository;
import com.edso.pawnservice.Service.CryptoService;
import com.edso.pawnservice.Service.Mapper.OfferMapper;
import com.edso.pawnservice.Service.OfferService;
import com.edso.pawnservice.Service.Validate.OfferValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OfferServiceImpl implements OfferService {
    @Autowired
    private OfferMapper offerMapper;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private OfferValidator validator;

    /**
     * Hiện thông tin offer theo id
     *
     * @param id
     * @return
     */
    @Override
    public OfferResponse getById(Long id) {
        Optional<Offer> optionalOffer = offerRepository.findById(id);
        if (optionalOffer.isEmpty()) {
            throw new CustomException("Resource offer not found");
        }
        Offer offer = optionalOffer.get();
        return offerMapper.mapToResponse(offer);
    }

    /**
     * Tạo mới Offer
     *
     * @param offerRequest
     * @return
     */
    @Override
    public OfferResponse createOffer(OfferRequest offerRequest) {
        validator.validate(offerRequest);
        Offer offer = offerMapper.mapToOfferEntity(offerRequest);
        offerRepository.save(offer);
        return new OfferResponse();
    }

}
