package com.edso.pawnservice.Service.Mapper;

import com.edso.pawnservice.Common.CollateralStatus;
import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Request.CollateralRequest;
import com.edso.pawnservice.Model.Response.CollateralResponse;
import com.edso.pawnservice.Service.CollateralService;
import com.edso.pawnservice.Service.Implements.CryptoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class CollateralMapper {

    @Autowired
    private OfferMapper offerMapper;

    @Autowired
    private CollateralService collateralService;

    @Autowired
    CryptoServiceImpl cryptoService;

    public List<CollateralResponse> mapToListResponse(List<Collateral> collaterals) {
        List<CollateralResponse> collateralResponsesList = new ArrayList<>();
        collaterals.forEach(collateral -> collateralResponsesList.add(mapToCollateralResponse(collateral)));
        return collateralResponsesList;
    }

    /**
     * convert dữ liệu từ entity sang Response
     *
     * @param collateral
     * @return
     */
    public CollateralResponse mapToCollateralResponse(Collateral collateral) {
        CollateralResponse collateralResponse = new CollateralResponse();
        collateralResponse.setId(collateral.getId());
        collateralResponse.setStatus(collateral.getStatus());
        collateralResponse.setMessage(collateral.getMessage());
        collateralResponse.setCollateralAmount(collateral.getCollateralAmount());
        collateralResponse.setDurationTime(collateral.getDurationTime());
        collateralResponse.setDurationType(collateral.getDurationType());
        collateralResponse.setWalletAddress(collateral.getWalletAddress());
        collateralResponse.setCurrencyId(collateral.getCryptoCurrency().getId());
        collateralResponse.setLoanCurrencyId(collateral.getLoanCurrency().getId());
        collateralResponse.setEstimate(collateralService.evaluateEstimate(collateral));
        if(!Objects.isNull(collateral.getOffers())){
            collateralResponse.setOffers(offerMapper.mapToList(collateral.getOffers()));
        }
        return collateralResponse;
    }

    /**
     * Convert dữ liệu từ Request sang Entity
     *
     * @param collateralRequest
     * @return
     */
    public Collateral mapToCollateral(CollateralRequest collateralRequest) {
        Collateral collateral = new Collateral();
//        BeanUtils.copyProperties(collateralRequest, collateral);
        collateral.setStatus(collateralRequest.getStatus());
        collateral.setCollateralAmount(collateralRequest.getCollateralAmount());
        collateral.setMessage(collateralRequest.getMessage());
        collateral.setDurationTime(collateralRequest.getDurationTime());
        collateral.setDurationType(collateralRequest.getDurationType());
        collateral.setCryptoCurrency(cryptoService.findCryptoWithId(collateralRequest.getCryptoCurrencyId()));
        collateral.setLoanCurrency(cryptoService.findCryptoWithId(collateralRequest.getLoanCurrencyId()));
        collateralRequest.setStatus(CollateralStatus.OPEN.toString());
        return collateral;
    }
}
