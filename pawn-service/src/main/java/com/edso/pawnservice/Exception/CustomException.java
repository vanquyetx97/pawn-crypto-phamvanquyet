package com.edso.pawnservice.Exception;

public class CustomException extends RuntimeException {
    public CustomException(String s) {
        super(s);
    }
}
