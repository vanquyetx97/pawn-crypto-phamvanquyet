package com.edso.pawnservice.Controller;

import com.edso.pawnservice.Model.Request.OfferRequest;
import com.edso.pawnservice.Model.Response.OfferResponse;
import com.edso.pawnservice.Model.Response.ResponseEntity;
import com.edso.pawnservice.Service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("offer")
public class OfferController {

    @Autowired
    private OfferService offerService;

    @GetMapping({"/{id}"})
    public ResponseEntity getById(@PathVariable long id) {
        OfferResponse offerResponse = offerService.getById(id);
        return new ResponseEntity(200, "Success", offerResponse);
    }

    @PostMapping
    public ResponseEntity createOffer(@RequestBody OfferRequest offerRequest) {
        OfferResponse offerCreated = offerService.createOffer(offerRequest);
        return new ResponseEntity(201, "Success", offerCreated);
    }

}
