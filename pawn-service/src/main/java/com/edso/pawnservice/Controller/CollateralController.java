package com.edso.pawnservice.Controller;

import com.edso.pawnservice.Model.Request.CollateralRequest;
import com.edso.pawnservice.Model.Response.CollateralResponse;
import com.edso.pawnservice.Model.Response.ResponseEntity;
import com.edso.pawnservice.Service.CollateralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("collateral")
public class CollateralController {

    @Autowired
    CollateralService collateralService;

    @GetMapping
    public ResponseEntity getListCollateral(@RequestParam(value = "pageIndex", defaultValue = "0",
                                                    required = false) int pageIndex,
                                            @RequestParam(value = "pageSize", defaultValue = "5",
                                                    required = false) int pageSize) {
        List<CollateralResponse> collateral = collateralService.getAll(pageIndex, pageSize);
        return new ResponseEntity(200, "Success", collateral);
    }

    @GetMapping({"/{id}"})
    public ResponseEntity getById(@PathVariable Long id) {
        CollateralResponse collateralResponse = collateralService.getById(id);
        return new ResponseEntity(200, "Success", collateralResponse);
    }

    @GetMapping({"/filter"})
    public ResponseEntity filterCollateral(@RequestParam(value = "wallet_address") String walletAddress,
                                           @RequestParam(value = "loan_currency") Long LoanCurrencyCryptoId,
                                           @RequestParam(value = "collateral") Long collateralCryptoId,
                                           @RequestParam(value = "status") String status,
                                           @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                           @RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {
        List<CollateralResponse> filteredCollateral = collateralService.filterCollateral(walletAddress,
                LoanCurrencyCryptoId, collateralCryptoId, status, pageIndex, pageSize);
        return new ResponseEntity(200, "Success", filteredCollateral);
    }

    @PostMapping
    public ResponseEntity createCollateral(@RequestBody @Valid CollateralRequest collateralRequest) {
        CollateralRequest collateral = collateralService.createCollateral(collateralRequest);
        return new ResponseEntity(201, "Success", collateral);
    }

    @PatchMapping({"/withDrawn/{id}"})
    public CollateralResponse withDrawCollateral(@PathVariable Long id) {
        return collateralService.withDrawCollateral(id);
    }

    @PutMapping({"/{id}"})
    public CollateralResponse updateCollateral(@PathVariable Long id,
                                               @RequestBody @Valid CollateralRequest collateralRequest) {
        return collateralService.updateCollateralById(id, collateralRequest);
    }

    @PatchMapping({"/{collateralId}/accept/{offerId}"})
    public ResponseEntity acceptOfferForCollateral(@PathVariable Long offerId, @PathVariable Long collateralId) {
        CollateralResponse collateralResponse = collateralService.acceptOfferForCollateral(collateralId, offerId);
        return new ResponseEntity(200, "Accept", collateralResponse);
    }

}
