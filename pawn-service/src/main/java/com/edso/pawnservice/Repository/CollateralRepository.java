package com.edso.pawnservice.Repository;

import com.edso.pawnservice.Model.Entity.Collateral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CollateralRepository extends JpaRepository<Collateral, Long>, JpaSpecificationExecutor<Collateral> {

}
