package com.edso.pawnservice.Repository;

import com.edso.pawnservice.Model.Entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long> {

    Crypto findCryptoBySymbol(String symbol);

}
