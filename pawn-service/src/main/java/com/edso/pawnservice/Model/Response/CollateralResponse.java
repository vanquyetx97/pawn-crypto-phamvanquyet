package com.edso.pawnservice.Model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralResponse {

    private long id;
    private BigDecimal collateralAmount;
    private String message;
    private int durationTime;
    private String durationType;
    private String status;
    private String walletAddress;
    private List<OfferResponse> offers;
    private long currencyId;
    private long loanCurrencyId;
    private BigDecimal estimate;


}
