package com.edso.pawnservice.Model.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "crypto")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Crypto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "int default 0")
    private long id;

    @Column(columnDefinition = "VARCHAR(100)")
    private String symbol;

    @Column(columnDefinition = "VARCHAR(100)")
    private String address;

    @CreatedDate
    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @Column(columnDefinition = "timestamp")
    private OffsetDateTime updatedAt;

    @Column(columnDefinition = "VARCHAR(100)")
    private String isDeleted;

    @Column(columnDefinition = "boolean default true")
    private boolean whitelistCollateral;

    @Column(columnDefinition = "boolean default true")
    private boolean whitelistSupply;

    @Column(columnDefinition = "VARCHAR(100)")
    private String name;

    @Column(columnDefinition = "VARCHAR(100)")
    private String coinGeckoId;

    @Column(columnDefinition = "VARCHAR(100)")
    private String url;

    @OneToMany(mappedBy = "cryptoCurrency", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Collateral> currencyCollateral;

    @OneToMany(mappedBy = "loanCurrency", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Collateral> cryptoLoanCurrency;

}
