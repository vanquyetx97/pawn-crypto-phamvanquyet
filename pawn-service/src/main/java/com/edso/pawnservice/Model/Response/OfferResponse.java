package com.edso.pawnservice.Model.Response;

import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Entity.Crypto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferResponse {

    private Long id;
    private String message;
    private String status;
    private Integer durationTime;
    private String durationType;
    private String walletAddress;
    private BigDecimal loanToValue;
    private BigDecimal loanAmount;
    private Double interestRate;
    private Double liquidationThreshold;
    private OffsetDateTime createdAt;
    private Long cryptoId;
    private Long collateralId;

}
