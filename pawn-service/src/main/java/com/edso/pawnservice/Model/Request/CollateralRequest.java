package com.edso.pawnservice.Model.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralRequest {
    @NotBlank
    private BigDecimal collateralAmount;

    @NotNull
    private long cryptoCurrencyId;

    @NotBlank
    private String status;

    @NotBlank
    private String message;

    @NotNull
    private int durationTime;

    @NotBlank
    private String durationType;

    @NotBlank
    private long loanCurrencyId;
}
