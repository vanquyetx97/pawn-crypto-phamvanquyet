package com.edso.pawnservice.Model.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferRequest {

    @NotBlank
    private String status;

    @NotBlank
    private String message;

    @NotBlank
    private BigDecimal loanToValue;

    @NotBlank
    @Max(20)
    private BigDecimal loanAmount;

    @NotNull
    private Double liquidationThreshold;

    @NotNull
    private Double interestRate;

    @NotNull
    private int durationTime;

    @NotBlank
    private String durationType;

    @NotBlank
    private String walletAddress;

    @NotNull
    private Long cryptoId;

    @NotNull
    private Long collateralId;
}
