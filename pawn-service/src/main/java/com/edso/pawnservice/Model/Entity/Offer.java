package com.edso.pawnservice.Model.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Date;

@Entity
@Table(name = "offer")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "TEXT(100)")
    private String message;

    @Column(columnDefinition = "VARCHAR(100)")
    private String status;

    @Column(columnDefinition = "Text(10)")
    private BigDecimal loanToValue;

    @Column(columnDefinition = "Text(20)")
    private BigDecimal loanAmount;

    @Column(columnDefinition = "Double precision")
    private Double liquidationThreshold;

    @Column(columnDefinition = "Double precision")
    private Double interestRate;

    @Column(columnDefinition = "int default 0")
    private int durationTime;

    @Column(columnDefinition = "VARCHAR(100)")
    private String durationType;

    @Column(columnDefinition = "VARCHAR(100)")
    private String repaymentCurrency;

    @Column(columnDefinition = "VARCHAR(100)")
    private String recurringInterest;

    @CreatedDate
    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt;

    @Column(columnDefinition = "VARCHAR(100)")
    private String walletAddress;

    @ManyToOne
    @JoinColumn(name = "collateral_id", referencedColumnName = "id")
    @JsonIgnore
    private Collateral collateral;

    @ManyToOne
    @JoinColumn(name = "crypto_id", referencedColumnName = "id")
    @JsonIgnore
    private Crypto crypto;

    @PrePersist
    void onCreate(){
        if (this.getCreatedAt() == null) this.setCreatedAt(OffsetDateTime.now());
    }

}
