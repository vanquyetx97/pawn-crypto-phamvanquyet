package com.edso.pawnservice.Model.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "collateral")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Collateral {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private BigDecimal collateralAmount;

    @Column(columnDefinition = "TEXT(100)")
    private String status;

    @Column(columnDefinition = "VARCHAR(100)")
    private String message;

    @Column(columnDefinition = "VARCHAR(100)")
    private String walletAddress;

    @Column(columnDefinition = "int default 0")
    private int durationTime;

    @Column(columnDefinition = "VARCHAR(10)")
    private String durationType;

    @ManyToOne
    @JoinColumn(name = "crypto_currency_id", referencedColumnName = "id")
    @JsonIgnore
    private Crypto cryptoCurrency;

    @ManyToOne
    @JoinColumn(name = "crypto_loan_currency_id", referencedColumnName = "id")
    @JsonIgnore
    private Crypto loanCurrency;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "collateral", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Offer> offers;

}
