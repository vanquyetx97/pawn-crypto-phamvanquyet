package com.edso.pawnservice.Model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CryptoResponse {
    private int id;
    private String symbol;
    private String url;
    private Boolean whitelistCollateral;
    private Boolean whitelistSupply;
}
