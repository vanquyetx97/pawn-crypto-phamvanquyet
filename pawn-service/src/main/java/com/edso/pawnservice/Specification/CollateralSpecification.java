package com.edso.pawnservice.Specification;

import com.edso.pawnservice.Model.Entity.Collateral;
import com.edso.pawnservice.Model.Entity.Crypto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.criteria.Predicate;

public class CollateralSpecification {
    /**
     * Hàm lọc theo trường walletAddress, cryptoCurrency, cryptoLoanCurrency, status
     * @param walletAddress
     * @param loanCurrencyId
     * @param collateralId
     * @param status
     * @return
     */
    public static Specification<Collateral> filterForCollateral(String walletAddress, Long loanCurrencyId,
                                                                Long collateralId, String status){
        return (root, query, criteriaBuilder) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            if(! Objects.isNull(walletAddress)){
                predicates.add(criteriaBuilder.equal(root.get("walletAddress"),walletAddress));
            }
            if (!Objects.isNull(loanCurrencyId)){
                Path<Crypto> cryptoPath = root.get("cryptoCurrency");
                predicates.add(criteriaBuilder.equal(cryptoPath.get("id"),loanCurrencyId));
            }
            if(!Objects.isNull(collateralId)){
                Path<Crypto> cryptoPath = root.get("cryptoLoanCurrency");
                predicates.add(criteriaBuilder.equal(cryptoPath.get("id"),loanCurrencyId));
            }
            if(!Objects.isNull(status)){
                predicates.add(criteriaBuilder.equal(root.get("status"),status));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

}
