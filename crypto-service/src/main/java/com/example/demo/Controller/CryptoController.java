package com.example.demo.Controller;

import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import com.example.demo.Service.CryptoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("crypto")
public class CryptoController {

    @Autowired
    private CryptoService cryptoService;

    @GetMapping
    public List<CryptoResponse> getListCrypto(@RequestParam(defaultValue = "0") int pageIndex,
                                              @RequestParam(defaultValue = "5") int pageSize) {
        return cryptoService.findAllCrypto(pageIndex, pageSize);
    }

    @GetMapping({"/keyword"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoResponse> getCryptoBySymbol(@RequestParam(value = "name", required = false) String name,
                                                  @RequestParam(value = "address", required = false) String address,
                                                  @RequestParam(value = "symbol", required = false) String symbol,
                                                  @RequestParam(defaultValue = "0") int pageIndex,
                                                  @RequestParam(defaultValue = "5") int pageSize) {
        return cryptoService.findByNameAndAddressAndSymbol(name, address, symbol, pageIndex, pageSize);
    }

    @GetMapping({"/current-price"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getCurrentPriceOfSymbol(@RequestParam(value = "symbol") String symbol) {
        CryptoExchangeResponse exchangeResponse = cryptoService.getCurrentPriceOfSymbol(symbol);
        return new ResponseEntity(200,"Success",exchangeResponse);
    }


    @GetMapping({"/price"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getPriceOfSymbolAtTime(@RequestParam (value = "symbol")String symbol,
                                                 @RequestParam(value = "time_at")Long time){
        return cryptoService.getPriceOfSymbolAtTime(symbol, time);
    }


    @PostMapping
    public ResponseEntity createCrypto(@RequestBody CryptoRequest crypto) {
        CryptoRequest cryptoRequest = cryptoService.createCrypto(crypto);
        return new ResponseEntity(200, "Success", cryptoRequest);
    }

    @PutMapping("{id}")
    public CryptoResponse updateCrypto(@PathVariable(value = "id") long id,
                                                 @RequestBody CryptoRequest cryptoRequest) {
        return cryptoService.updateCryptoById(id, cryptoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCrypto(@PathVariable long id) {
        cryptoService.deleteById(id);
    }

    @GetMapping({"/exchange"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Convert amount from crypto A to crypto B")
    public ResponseEntity exchangeCrypto(@RequestParam(value = "fromSymbol") String fromSymbol,
                                        @RequestParam(value = "fromAmount") BigDecimal fromAmount,
                                        @RequestParam(value = "toSymbol") String toSymbol){
        BigDecimal priceAfterExchange = cryptoService.exchangeSymbolTo(fromSymbol,fromAmount,toSymbol);
        return new ResponseEntity(200,"\n" +
                "Exchange rate after conversion",priceAfterExchange);
    }

    @GetMapping({"/whitelist-collateral"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get a list of collateral")
    public ResponseEntity whiteListCollateral(){
        List<CryptoResponse> whiteListCollateral = cryptoService.currencyCollateralAvailable();
        return new ResponseEntity(200,"Success",whiteListCollateral);
    }

    @GetMapping({"/whitelist-supply"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get a list of loan currencies")
    public ResponseEntity whiteListSupply(@RequestParam(value = "collateralId") Long collateralId){
        List<CryptoResponse> whiteListSupply = cryptoService.loanCurrencyAvailable(collateralId);
        return new ResponseEntity(200,"Loan currency available",whiteListSupply);
    }

}
