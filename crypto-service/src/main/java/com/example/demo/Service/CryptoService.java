package com.example.demo.Service;

import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface CryptoService {

    List<CryptoResponse> findAllCrypto(int pageIndex, int pageSize);

    CryptoRequest createCrypto(CryptoRequest cryptoRequest);

    CryptoResponse updateCryptoById(long id, CryptoRequest cryptoRequest);

    void deleteById(long id);

    List<CryptoResponse> findByNameAndAddressAndSymbol(String name, String address, String symbol, int pageIndex, int pageSize);

    List<CryptoExchangeResponse> getCurrentPriceOfSymbolList(String ids);

    BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol);

    ResponseEntity getPriceOfSymbolAtTime(String symbol, Long timeAt);

    List<CryptoResponse> currencyCollateralAvailable();

    List<CryptoResponse> loanCurrencyAvailable(Long id);

    CryptoExchangeResponse getCurrentPriceOfSymbol(String symbol);

    List<String> getAllCoinGeckoId();


}
