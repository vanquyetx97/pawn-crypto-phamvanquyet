package com.example.demo.Service;

import com.example.demo.Exception.CustomException;
import com.example.demo.Model.Entity.Crypto;
import com.example.demo.Model.Entity.CryptoRateHistory;
import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import com.example.demo.Repository.CryptoRateHistoryRepository;
import com.example.demo.Repository.CryptoRepository;
import com.example.demo.Service.Mapper.CryptoMapper;
import com.example.demo.Specification.CryptoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CryptoServiceImpl implements CryptoService {

    @Autowired
    private CryptoMapper cryptoMapper;

    @Autowired
    private CryptoRepository cryptoRepository;

    @Autowired
    private CryptoRateHistoryRepository cryptoRateHistoryRepository;

    private WebClient webClient;

    private final String COIN_GECKO_URL = "https://api.coingecko.com";
    private final String COIN_GECKO_BY_IDS =
            "/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&ids={listID}";


    /**
     * Lấy all dữ liệu
     *
     * @return
     */
    @Override
    public List<CryptoResponse> findAllCrypto(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> cryptoPage = cryptoRepository.findAll(paging);
        List<Crypto> cryptoList = cryptoPage.getContent();
        return cryptoMapper.map(cryptoList);

    }

    /**
     * Tìm kiếm theo name, address, symbol
     *
     * @param name
     * @param address
     * @param symbol
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CryptoResponse> findByNameAndAddressAndSymbol(String name,
                                                              String address,
                                                              String symbol,
                                                              int pageIndex,
                                                              int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> page = cryptoRepository.findAll(CryptoSpecification.fineCrypto(name, address, symbol), pageable);
        List<Crypto> cryptos = page.getContent();
        return cryptoMapper.map(cryptos);

    }

    /**
     * Lấy Current Price theo ids
     *
     * @param ids
     * @return
     */
    @Override
    public List<CryptoExchangeResponse> getCurrentPriceOfSymbolList(String ids) {
        List<CryptoExchangeResponse> response = webClient.get()
                .uri(COIN_GECKO_BY_IDS, ids)
                .retrieve()
                .bodyToFlux(CryptoExchangeResponse.class)
                .collectList()
                .block();
        return response;
    }

    /**
     * chuyển đổi giữa 2 cryptocurrency
     *
     * @param fromSymbol
     * @param fromAmount
     * @param toSymbol
     * @return
     */
    @Override
    public BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol) {
        Crypto fromCrypto = cryptoRepository.findBySymbol(fromSymbol);
        Crypto toCrypto= cryptoRepository.findBySymbol(toSymbol);
        if(Objects.isNull(fromCrypto) || Objects.isNull(toCrypto)){
            throw new CustomException("Invalid input crypto");
        }
        BigDecimal fromSymbolPrice,toSymbolPrice;
        // tỷ giá mới của 2 crypto
        fromSymbolPrice = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(fromCrypto.getId()).getCurrentPrice();
        toSymbolPrice = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(toCrypto.getId()).getCurrentPrice();
        BigDecimal toAmount = null;
        if (!Objects.isNull(fromSymbolPrice) && !Objects.isNull(toSymbolPrice)) {
            toAmount = fromAmount.multiply(fromSymbolPrice.divide(toSymbolPrice,5, RoundingMode.CEILING));
        }
        return toAmount;

    }


    /**
     * Lấy tất cả Coin Gecko theo id
     *
     * @return
     */
    @Override
    public List<String> getAllCoinGeckoId() {
        List<Crypto> cryptos = cryptoRepository.findAll();
        List<String> symbols = cryptos.stream().map(crypto -> crypto.getCoinGeckoId()).collect(Collectors.toList());
        return symbols;
    }

    /**
     * @param symbol
     * @param timeAt
     * @return
     */
    @Override
    public ResponseEntity getPriceOfSymbolAtTime(String symbol, Long timeAt) {
        Long crypto_id = cryptoRepository.findBySymbol(symbol).getId();
        BigDecimal priceAtTime = cryptoRateHistoryRepository.getPriceAtTimeNearest(crypto_id, timeAt);
        return new ResponseEntity(200, "Price at: ", priceAtTime);
    }


    /**
     * Tạo Crypto dạng boolean, tạo mới được thì trả về true
     *
     * @param cryptoRequest
     * @return
     */
    @Override
    public CryptoRequest createCrypto(CryptoRequest cryptoRequest) {
        Crypto crypto = cryptoMapper.map(cryptoRequest);
        cryptoRepository.save(crypto);
        return new CryptoRequest();
    }

    /**
     * Update Crypto PathVariable theo id
     *
     * @param id
     * @param cryptoRequest
     * @return
     */
    @Override
    public CryptoResponse updateCryptoById(long id, CryptoRequest cryptoRequest) {
        Crypto crypto = cryptoMapper.map(cryptoRequest);
        crypto.setId(id);
        return saveAndReturnResponse(crypto);
    }

    /**
     * Lưu dữ liệu của Entity khi Response trả về
     *
     * @param crypto
     * @return
     */
    private CryptoResponse saveAndReturnResponse(Crypto crypto) {
        Crypto saveCrypto = cryptoRepository.save(crypto);
        return cryptoMapper.map(saveCrypto);
    }

    /**
     * Delete Crypto theo id
     *
     * @param id
     */
    @Override
    public void deleteById(long id) {
        cryptoRepository.deleteById(id);
    }

    @Override
    public CryptoExchangeResponse getCurrentPriceOfSymbol(String symbol) {
        // lấy ra crypto có symbol cần tìm
        Crypto crypto = cryptoRepository.findBySymbol(symbol);
        CryptoExchangeResponse cryptoExchangeDTO = new CryptoExchangeResponse();
        cryptoExchangeDTO.setSymbol(symbol);
        cryptoExchangeDTO.setName(crypto.getName());
        // lấy tỷ giá mới của crypto
        CryptoRateHistory cryptoRateHistory = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(crypto.getId());
        cryptoExchangeDTO.setCurrentPrice(cryptoRateHistory.getCurrentPrice());
        return cryptoExchangeDTO;
    }

    /**
     * Nhận danh sách tài sản thế chấp
     *
     * @return
     */
    @Override
    public List<CryptoResponse> currencyCollateralAvailable() {
        List<Crypto> cryptoList = cryptoRepository.findByWhitelistCollateralTrue();
        return cryptoMapper.map(cryptoList);
    }

    /**
     * Nhận danh sách các loại tiền cho vay
     *
     * @param id
     * @return
     */
    @Override
    public List<CryptoResponse> loanCurrencyAvailable(Long id) {
        List<Crypto> cryptoList = cryptoRepository.findByWhitelistSupplyTrueAndIdNot(id);
        return cryptoMapper.map(cryptoList);
    }
}
