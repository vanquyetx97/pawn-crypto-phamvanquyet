package com.example.demo.Repository;

import com.example.demo.Model.Entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long>, JpaSpecificationExecutor<Crypto> {


    Crypto findBySymbol(String symbol);

    List<Crypto> findByWhitelistCollateralTrue();

    List<Crypto> findByWhitelistSupplyTrueAndIdNot(Long id);

}
