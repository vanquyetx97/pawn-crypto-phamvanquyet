package com.example.demo.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {

    public static String convertListToString(List<String> strings){
        return strings.stream().collect(Collectors.joining(","));
    }
}
