package com.example.demo.Model.Entity;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.OffsetDateTime;

@Entity
@Table(name = "crypto_rate_history")
public class CryptoRateHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "current_price")
    private BigDecimal currentPrice;

    @CreatedDate
    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "crypto_id")
    private Crypto crypto;

    public CryptoRateHistory() {
        //Init constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal current_price) {
        this.currentPrice = current_price;
    }

    public OffsetDateTime getCreateAt() {
        return createdAt;
    }

    public void setCreateAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    @PrePersist
    void onCreate() {
        this.setCreateAt(OffsetDateTime.now());
    }
}
